#include "utils.h"
#include <cassert>
#include <limits>

struct Pair {
    double max;
    int idx;
};

/// Find the maximum value `a` among all warps and return {max value, index of
/// the max}. The result must be correct on at least the 0th thread of each warp.
__device__ Pair argMaxWarp(double a) {
    // TODO: 1.b) Compute the argmax of the given value.
    //            Return the maximum and the location of the maximum (0..31).
    int warpid = threadIdx.x & 31;
    Pair result;
    result.max = a;
    result.idx = warpid;

    Pair tmp;
    
    for(int i = 1; i < 32; i <<= 1){
        tmp.max = __shfl_down_sync(0xffffffff, result.max, i, warpSize);
        tmp.idx = __shfl_down_sync(0xffffffff, redult.idx, i, warpSize);

        if(tmp.max > result.max && warpid+1 < 32){
            result = tmp;
        }
    }

    return result;
}


/// Returns the argmax of all values `a` within a block,
/// with the correct answer returned at least by the 0th thread of a block.
__device__ Pair argMaxBlock(double a) {
    // TODO: 1.c) Compute the argmax of the given value.
    //            Return the maximum and the location of the maximum (0..1023).
    // NOTE: For 1.c) implement either this or `sumBlock`!
    Pair result;
    result.max = 0.0;
    result.idx = 0;

    // ...

    return result;
}

__global__ void argMax1MKernel(const double *a, double *b, double *blockargMax, int N) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    double argMax = argMaxBlock(idx < N ? a[idx] : 0.0);
    if (threadIdx.x == 0)
        blockargMax[blockIdx.x] = argMax;

    if(blockIdx.x == 0){
        blockargMax[0] = argMaxBlock(argMax);
        if(threadIdx.x == 0){
            b[0] = argMax;
            printf("b[0] = %f", argMax);
        }
    }

}

void argMax1M(const double *aDev, Pair *bDev, int N) {
    assert(N <= 1024 * 1024);
    // TODO: 1.d) Implement either this or `sum1M`.
    //            Avoid copying any data back to the host.
    //            Hint: The solution requires more CUDA operations than just
    //            calling a single kernel. Feel free to use whatever you find
    //            necessary.
    int threadsPerBlock = 1024;
    int numBlocks = (N+threadsPerBlock-1) / threadsPerBlock;
    printf("launching with <<<%i, %i>>>\n", numBlocks, threadsPerBlock);
    double *blockargMax;
    CUDA_CHECK(cudaMalloc(&blockargMax, numBlocks * sizeof(double)));
    argMax1MKernel<<<numBlocks, threadsPerBlock>>>(aDev, bDev, blockargMax, N);
}

#include "reduction_argmax.h"

int main() {
    testSmallArgMax(argMaxWarpTestKernel, argMaxWarpCheck, 32, 3);
    testSmallArgMax(argMaxWarpTestKernel, argMaxWarpCheck, 32, 32);
    testSmallArgMax(argMaxWarpTestKernel, argMaxWarpCheck, 32, 320);
    testSmallArgMax(argMaxWarpTestKernel, argMaxWarpCheck, 32, 1023123);
    printf("argMaxWarp OK.\n");

    testSmallArgMax(argMaxBlockTestKernel, argMaxBlockCheck, 1024, 32);
    testSmallArgMax(argMaxBlockTestKernel, argMaxBlockCheck, 1024, 1024);
    testSmallArgMax(argMaxBlockTestKernel, argMaxBlockCheck, 1024, 12341);
    testSmallArgMax(argMaxBlockTestKernel, argMaxBlockCheck, 1024, 1012311);
    printf("argMaxBlock OK.\n");

    testLargeArgMax("argMax1M", argMax1M, 32);
    testLargeArgMax("argMax1M", argMax1M, 1024);
    testLargeArgMax("argMax1M", argMax1M, 12341);
    testLargeArgMax("argMax1M", argMax1M, 1012311);
    printf("argMax1M OK.\n");
}

