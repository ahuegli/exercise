#include "utils.h"
#include <numeric>
#include <omp.h>
#include <vector>

using ll = long long;

// Compute the sum of the Leibniz series. Each thread takes care of a subset of terms.
__global__ void leibnizKernel(ll K, double *partialSums) {
    int idx = blockIdx.x * blockDim.x + threadIdx.x;
    double sum = 0.0;

    double sign;
	ll iterPerBlock  = K / gridDim.x;

    ll iterPerThread = iterPerBlock / blockDim.x;

    ll startIdx = idx * iterPerThread;
	ll endIdx = min(K, startIdx + iterPerThread);

    for(int i = startIdx; i < endIdx; ++i){
        if(i%2)
            sign = -1.0;
        else
            sign = 1.0
		
		sum += sign / (2*i + 1);
	}


    // TODO: Compute the partial sum. Pick however you like which terms are computed by which thread.
    //       Avoid using std::pow for computing (-1)^k!

    partialSums[idx] = sum;
}

/// Run the CUDA code for the given number of blocks and threads/block.
void runCUDA(ll K, int numBlocks, int threadsPerBlock) {
    int numThreads = numBlocks * threadsPerBlock;

    // Allocate the device and host buffers.

    double *partialSumsDev;
    double *partialSumsHost;

    // TODO: Allocate the temporary buffers for partial sums.
    cudaMallocHost(&partialSumsHost, numThreads * sizeof(double));
	cudaMalloc(&partialSumsDev, numThreads * sizeof(double));



    // TODO: Run the kernel and benchmark execution time.
    double dt = 1e-9 * benchmark(1, leibnizKernel, numBlocks, threadsPerBlock, true, K, partialSumsDev);


    // TODO: Copy the sumsDev to host and accumulate, and sum them up.
    double sum = 0.0;
	for(int i = 0; i < numThreads; ++i){
		sum += partialSumsHost[i];
	}


    double pi = 4 * sum;
    printf("CUDA blocks=%5d  threads/block=%4d  iter/thread=%5lld  pi=%.12f  rel error=%.2g  Gterms/s=%.1f\n",
           numBlocks, threadsPerBlock, K / numThreads, pi, (pi - M_PI) / M_PI,
           1e-9 * K / dt);

    // TODO: Deallocate cuda buffers.
    cudaFree(partialSumsDev);
	cudaFree(partialSumsHost);

}

/// Run the OpenMP variant of the code.
void runOpenMP(ll K, int numThreads) {
    double sum = 0.0;

    // TODO: Implement the Leibniz series summation with OpenMP.
    auto t0 = std::chrono::high_resolution_clock::now();
    omp_set_num_threads(numThreads);

    #pragma omp parallel for reduction(+ : sum)
    for(int i = 0; i < K; ++i){
        if(i%2)
            sign = -1.0;
        else
            sign = 1.0;

        sum += sign / (2*i + 1);
    }
    auto t1 = std::chrono::high_resolution_clock::now();


    // TODO: Benchmark execution time.
    double dt = 0.0;
    std::chrono::duration<double, std::nano> dt = t1 - t0;


    double pi = 4 * sum;
    printf("OpenMP threads=%d  pi=%.16g  rel error=%.2g  Gterms/s=%.1f\n",
           numThreads, pi, (pi - M_PI) / M_PI, 1e-9 * K / dt);
};


void subtask_c() {
    constexpr ll K = 2LL << 30;

    // TODO: Experiment with number of threads per block, and number of blocks
    // (i.e. number of iterations per thread).
    runCUDA(K, 512, 512);

    runOpenMP(K, 12);
}

int main() {
    subtask_c();
}
